package com.chen.shiro;

import com.chen.shiro.pojo.User;
import com.chen.shiro.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class ShiroApplicationTests {
    @Autowired
    UserService userService;

    @Test
    void contextLoads() {
        List<User> list = userService.list();
        System.out.println(list);
    }

}
