package com.chen.shiro.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.shiro.pojo.User;

public interface UserService extends IService<User> {
}
