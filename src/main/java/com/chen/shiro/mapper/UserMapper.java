package com.chen.shiro.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.shiro.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
