package com.chen.shiro.config;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("getDefaultWebSecurityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(defaultWebSecurityManager);
        /*
        anon: 无需认证就可以访问
        authc: 必须认证了才能让问必须拥有记住我功能才能用
        user:perms: 拥有对某个资源的权限才能访问
        role:拥有某个角色权限才能访问
         */
        Map<String,String> map=new HashMap<>();
        map.put("/index/**","authc");
        bean.setFilterChainDefinitionMap(map);
        bean.setLoginUrl("/login.html");
        return bean;
    }
    @Bean(name = "getDefaultWebSecurityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("userRealm") UserRealm userRealm){

        return new DefaultWebSecurityManager(userRealm);
    }
    //创建Realm对象
    @Bean(name = "userRealm")
    public UserRealm userRealm(){
        return new UserRealm();
    }
}
