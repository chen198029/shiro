package com.chen.shiro.config;

import com.chen.shiro.pojo.User;
import com.chen.shiro.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserRealm extends AuthorizingRealm {
    @Autowired
    UserService userService;
    //授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
    //认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        UsernamePasswordToken token1 = (UsernamePasswordToken) token;
        String username = token1.getUsername();
        List<User> list = userService.list();
        if(list==null){
            throw new UnknownAccountException();
        }
        for (User user : list) {
            if(user.getUsername().equals(username)){
                return new SimpleAuthenticationInfo("",user.getPassword(),"");
            }
        }
        throw new UnknownAccountException();
    }
}
